<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# Seo Extractor  
Extracts SEO information from HTML content & sets it up to work with Liaison.  
  
With Liaison, simply do:  
```  
\Lia\Addon\SeoExtractor::enable($liaison);  
```  
  
Otherwise, see [code/SeoExtractor.php](/code/SeoExtractor.php) for more  
  
## Install  
```bash  
composer require taeluf/liaison.seo-extractor v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/liaison.seo-extractor": "v0.1.x-dev"}}  
```    
