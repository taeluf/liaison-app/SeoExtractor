# Seo Extractor
Extracts SEO information from HTML content & sets it up to work with Liaison.

With Liaison, simply do:
```
\Lia\Addon\SeoExtractor::enable($liaison);
```

Otherwise, see @see_file(code/SeoExtractor.php) for more

## Install
@template(composer_install,taeluf/liaison.seo-extractor)  
